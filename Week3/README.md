 # Week 3 - Multiple Filters and Doing Math
 
 ## This week, we:
 
-  Learned how to filter a dataframe on multiple criteria by enclosing each criterion in parentheses and combining with the `&` operator
-  Learned how to import a single function from a library
    -  In our case, this was the `floor` function from the `math` library.
-  Learned how the `floor` and `modulo` (`%`) functions work
    -  `floor` rounds numbers to the nearest _smaller_ integer
    -  `modulo` divides by a number, but keeps the _remainder_
-  Discussed how square brackets `[]`, like many symbols in Python, carry multiple meanings. In particular, they:
    -  Define filters on dataframes, e.g. `Songs[Songs['artist'] == '*NSYNC']`
    -  Define **lists**, which is a way of storing multiple values in one place, e.g. `['Harry', 'Ron', 'Hermione']`
