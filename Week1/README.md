# Week 1 - Intro to Pandas

## This week, we:

-   Learned what _libraries_ are, and that **Pandas** is a versatile one, indeed.
    -   Specifically, Pandas lets us operate with **DataFrames**, which are versatile, tabular representations of data - perfect for folks who traditionally existed in Spreadsheet Land.
-   Imported a `.csv` [file](./songs_normalize.csv) containing the top 2000 Spotify songs from the past two decades, as downloaded from [Kaggle](https://www.kaggle.com/datasets/paradisejoy/top-hits-spotify-from-20002019).
-   Filtered data based on a single-column condition
    -   Learned that equality is represented by `==`, whereas `=` is used for _assignment_
-   Counted rows to identify which artists had multiple songs in the playlist
-   Restricted our view to lists of columns
    -   And learned why multiple columns must be contained in a list
-   Retrieved single records by filtering, and by using `.loc`
-   Learned all about lists
    -   That they're a way of storing sets of data
    -   That they're indexed, starting from zero
-   Learned a little about text
    -   That a string of text can be treated as a list
    -   That the character _4_ is fundamentally different from the number `4`, insofar as _4_ multiplied by 2 produces _44_ and not `8`
-   Discussed how filters don't affect the original dataset; they must be saved to a new variable
-   Learned that variables are case-sensitive
