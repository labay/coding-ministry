# Week 2 - Operating on DataFrames

## This week, we:

-  Learned how to perform aggregate functions on a dataframe, such as `count()`
    -  And that, as with SQL, aggregation requires grouping. This is accomplished using the `groupby()` function.
-  Learned how to sort dataframes using the `sort_values()` function.
-  Discovered that operating on a dataframe doesn't actually operate on that dataframe - it just stores the instructions for doing so. This can have weird implications down the road. Therefore, if we intend to operate on a filtered dataframe, we should store a `copy()` first.
-  Performed math on individual columns to convert milliseconds into seconds.
